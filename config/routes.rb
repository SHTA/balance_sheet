Rails.application.routes.draw do
  root 'users#index'

  resources :users do
    resources :expenses, only: [:new, :create], controller: 'users/expenses'
    resources :incomes, only: [:new, :create], controller: 'users/incomes'
    member do
    end
  end
end