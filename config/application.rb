require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module BalanceSheet
  class Application < Rails::Application
    config.enable_dependency_loading = true
    config.i18n.default_locale = :ru
    config.i18n.available_locales = :ru
    config.generators.test_framework false
    config.eager_load_paths += %W(#{ config.root }/lib/custom)
  end
end
