class CurrencyHandler

  def self.ntc number
    helper.number_to_currency(number, unit: '')
  end

  def self.helper
    @helper ||= Class.new do
      include ActionView::Helpers::NumberHelper
    end.new
  end
end