class TimeHandler
  # Returns full years passed from definite date or nil if date doesn't present.
  def self.get_difference_in_years(date_from)
    if date_from.present?
      date_to = Date.today
      diff = date_to.year - date_from.year
      if (date_from.month > date_to.month) || (date_from.month >= date_to.month and date_from.day > date_to.day)
        diff -= 1
      end
      return diff
    else
      return nil
    end
  end

  # Get right form of noun depending on number of years
  def self.get_russian_years(num)
    return nil unless num.present?
    s = "#{ num }"
    ln = s[-1].to_i
    if num >= 11 && num <= 20
      return 'лет'
    end
    case ln
    when 1
      'год'
    when 2..4
      'года'
    else
      'лет'
    end
  end

  # Russian format for Datetime
  def self.rus_date date
    date.strftime('%d.%m.%Y') rescue date
  end

  def self.rus_dtime dtime
    dtime.strftime('%d.%m.%Y %H:%M') rescue dtime
  end
  def self.rus_time time
    time.strftime('%H:%M') rescue time
  end
end