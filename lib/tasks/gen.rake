namespace :gen do

  desc 'Генерация сотрудников'
  task :users, [:count] => :environment do |task,args|
    names_arr = ['Пётр', 'Олег', 'Василий', 'Александр', 'Алексей', 'Игорь', 'Дмитрий', 'Сергей', 'Егор', 'Леонид', 'Владимир']
    today = Date.today
    for i in 0..(args[:count].to_i-1)
      birthday = today - rand((18..55)).year - rand((1..365)).day
      user = User.new(
        name: "#{ names_arr.sample }-#{ birthday.year }",
        birthday: birthday
      )
      puts "Пользователь #{ user.name } (#{ TimeHandler.rus_date birthday }) создан" if user.save
    end
  end

  desc 'Генерация транзакций'
  task :transactions, [:count] => :environment do
    now = Time.now
    for user in User.all
      rand((2..5)).times do
        t = ['expenses','incomes'].sample
        transaction_time = (now - rand((1..50)).day - rand((1..500)).minute)
        transaction = user.send(t).new(
          amount: (rand((1..50)) * 1000),
          transaction_time: transaction_time
        )
        puts "Пользователь #{ user.name } совершил транзацию #{ TimeHandler.rus_dtime transaction_time }: #{ transaction.class } на сумму #{ transaction.amount }." if transaction.save
      end
    end
  end
end