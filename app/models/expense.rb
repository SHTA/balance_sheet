class Expense < ApplicationRecord
  include Transactionable

  belongs_to :user, inverse_of: :expenses
end
