class User < ApplicationRecord
  validates :name, presence: true, uniqueness: { case_sensitive: false }, length: { minimum: 3, maximum: 20 }, allow_blank: false
  validates :birthday, presence: true
  validate :birthday_is_in_past

  has_many :incomes, inverse_of: :user, dependent: :destroy
  has_many :expenses, inverse_of: :user, dependent: :destroy

  before_validation :strip_name_strings, on: [:create, :update]

  def age
    y = TimeHandler.get_difference_in_years(self.birthday)
    "#{ y } #{ TimeHandler.get_russian_years(y) }"
  end

  def in_month_transactions
    incomes = self.incomes.in_month
    expenses = self.expenses.in_month
    transactions = (incomes + expenses) rescue nil
  end

  def get_month_balance
    transactions = self.in_month_transactions
    (return 0) unless transactions.present?
    transactions.map { |t| (t.kind_of?(Income) ? t.amount : -t.amount) }.sum()
  end

  private
  # Альтернатива - сделать кастомный валидатор для временнЫх величин в app/validators/
  def birthday_is_in_past
    if birthday.present? && birthday >= Date.today
      errors.add(:birthday, "Нельзя зарегистрировать еще не родившегося пользователя!")
    end
  end

  # Не допускаем пробелы в конце и начале name.
  def strip_name_strings
    if self.changes[:name].present?
      self.name.strip!
    end
  end
end
