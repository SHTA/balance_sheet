class Income < ApplicationRecord
  include Transactionable

  belongs_to :user, inverse_of: :incomes
end
