module Transactionable
  extend ActiveSupport::Concern
  include ActiveModel::Validations

  included do
    validates :amount, presence: true, numericality: { greater_than: 0 }
    validates :transaction_time, presence: true
    validates :user, presence: true

    scope :in_month, -> { where('transaction_time > ? AND transaction_time < ?', (Date.today).beginning_of_month, (Date.today + 1.month).beginning_of_month) }
    scope :commited, -> { where('transaction_time <= ?', Time.now) }
  end
end