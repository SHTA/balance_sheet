class GetInfoForUserIndex
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def get_balances_hash
    transactions = Income.commited + Expense.commited
    res = transactions.group_by { |t| t.user_id }.transform_values! { |v| v.map { |t| (t.kind_of?(Income) ? t.amount : -t.amount) }.sum() } rescue {}
  end
end