######################################
# ДЛЯ SIMPLE-FORM
# ПРИМЕР ИСПОЛЬЗОВАНИЯ ВО ВЬЮХЕ (СМОТРИМ НА ПОСЛЕДНИЙ ПАРАМЕТР):
# = f.submit 'Сохранить', class: 'btn btn-default pull-right', data: {submit_confirm: 'true'}
######################################


$(document).on 'submit', 'form', (event) ->
  if $('[data-submit-confirm="true"]',@).length
    event.preventDefault()
    $('#confirmation_modal').modal('show')
    form = $(@)
    elem = event.target
    $('#accept_action').on 'click', () ->
      $('[data-submit-confirm="true"]').attr 'data-submit-confirm', 'false'
      form.submit()
    $('#confirmation_modal').on 'hidden.bs.modal', () ->
      $('input[data-submit-confirm="true"]').removeAttr 'disabled'

######################################
# ДЛЯ LINK_TO
# ПРИМЕР ИСПОЛЬЗОВАНИЯ ВО ВЬЮХЕ (СМОТРИМ НА ПОСЛЕДНИЙ ПАРАМЕТР):
# = link_to store_path(store), method: :delete, title: 'Удалить', class: 'btn btn-xs btn-danger', data: { confirm: "Are you sure you want to delete" }
######################################

# Override Rails handling of confirmation

$.rails.allowAction = (element) ->
  # The message is something like "Are you sure?"
  message = element.data('confirm')
  # If there's no message, there's no data-confirm attribute, 
  # which means there's nothing to confirm
  return true unless message
  # Clone the clicked element (probably a delete link) so we can use it in the dialog box.
  $link = element.clone()
    # We don't necessarily want the same styling as the original link/button.
    .removeAttr('class')
    # We don't want to pop up another confirmation (recursion)
    .removeAttr('data-confirm')
    # Change button text when it is clicked
    .attr('data-disable-with','В процессе...')
    # We want a button
    .addClass('btn').addClass('btn btn-lg btn-default flat')
    # Add id.
    .attr('id','accept_action')
    # We want it to sound confirmy
    .html("Да")

  # Create the modal box with the message
  modal_html = """
              <div class="modal fade in" id="confirmation_modal" role="dialog" tabindex="-1" aria-hidden="false" style="display: block; padding-right: 17px;"><div class="modal-dialog modal-xs"><div class="modal-content"><div class="modal-header bg-blue"><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button><h4 class="modal-title">#{message}</h4></div><div class="modal-body"><div class="row"><div class="col-sm-2 col-xs-1"></div><div class="col-sm-4 col-xs-5 for_accept"></div><div class="col-sm-4 col-xs-5"><button class="btn btn-lg btn-default flat pull-right" data-dismiss="modal" id="decline_submit">Нет</button></div></div></div></div></div></div>
               """
  $modal_html = $(modal_html)
  # Add the new button to the modal box
  $modal_html.find('.for_accept').append($link)
  # Pop it up
  $modal_html.modal()
  # Prevent the original link from working
  return false

$(document).on 'click','#accept_action', () ->
  $(@).attr 'disabled',true