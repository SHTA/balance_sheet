$ ->
  window.Datepicker.initDatepicker()

window.Datepicker ?= {}
window.Datepicker.initDatepicker = () ->
  $('.datepicker').datetimepicker({
    locale: 'ru',
    format: 'DD.MM.YYYY',
    showClear: true,
  })
  $('.timepicker').datetimepicker({
    locale: 'ru',
    format: 'HH:mm',
    showClear: true,
  })
  $('.datetimepicker').datetimepicker({
    locale: 'ru',
    format: 'DD.MM.YYYY HH:mm',
    showClear: true,
  })