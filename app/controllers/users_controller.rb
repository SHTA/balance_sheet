class UsersController < ApplicationController

  def show
    @user = User.find params[:id]
    @transactions = @user.in_month_transactions
  end

  def index
    @users = User.all.order(:name)
    service = GetInfoForUserIndex.new(params)
    @balances_hash = service.get_balances_hash
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash.notice = 'Пользователь добавлен'
      redirect_to root_path
    else
      flash.alert = 'Исправьте ошибки'
      render :new
    end
  end

  def edit
    @user = User.find params[:id]
  end

  def update
    @user = User.find params[:id]
    if @user.update(user_params)
      flash.notice = 'Информация о пользователе отредактирована'
      redirect_to root_path
    else
      flash.alert = 'Исправьте ошибки'
      render :edit
    end
  end

  def destroy
    @user = User.find params[:id]
    if @user.destroy
      flash.notice = 'Пользователь удален!'
    else
      flash.alert = "Пользователь не был удален!"
    end
    redirect_to root_path
  end

  private
  def user_params
    params.require(:user).permit(
      :name,
      :birthday,
      :address,
    )
  end

end