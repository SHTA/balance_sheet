class Users::ExpensesController < ApplicationController
  def new
    @user = User.find params[:user_id]
    @expense = @user.expenses.new
  end

  def create
    @user = User.find params[:user_id]
    @expense = @user.expenses.new
    amount = BigDecimal(expense_params[:amount]) rescue nil
    purpose = expense_params[:purpose] rescue ''
    user_id = params[:user_id].to_i rescue nil
    transaction_time = expense_params[:transaction_time].to_datetime.to_s rescue nil
    if amount.present? && user_id.present? && transaction_time.present?
      sql = "SELECT create_expense(#{ amount }, #{ user_id }, '#{ transaction_time }', '#{ purpose }');"
      records_array = ActiveRecord::Base.connection.execute(sql)
      flash.notice = 'Информация о расходе денежных средств зарегистрирована.'
      redirect_to user_path(params[:user_id])
    else
      flash.alert = 'Исправьте ошибки'
      render :new
    end
  end

  private
  def expense_params
    params.require(:expense).permit(
      :amount,
      :transaction_time,
      :user_id,
      :purpose,
    )
  end
end