class Users::IncomesController < ApplicationController
  def new
    @user = User.find params[:user_id]
    @income = @user.incomes.new
  end

  def create
    @user = User.find params[:user_id]
    @income = @user.incomes.new
    amount = BigDecimal(income_params[:amount]) rescue nil
    user_id = params[:user_id].to_i rescue nil
    transaction_time = income_params[:transaction_time].to_datetime.to_s rescue nil
    if amount.present? && user_id.present? && transaction_time.present?
      sql = "SELECT create_income(#{ amount }, #{ user_id }, '#{ transaction_time }');"
      records_array = ActiveRecord::Base.connection.execute(sql)
      flash.notice = 'Информация о поступлении денежных средств зарегистрирована.'
      redirect_to user_path(params[:user_id])
    else
      flash.alert = 'Исправьте ошибки'
      render :new
    end
  end

  private
  def income_params
    params.require(:income).permit(
      :amount,
      :transaction_time,
      :user_id,
    )
  end

end