## List of bash commands to implement.

```
docker-compose build
docker-compose run --rm -e EDITOR=vim app bin/rails credentials:edit
docker-compose run app rake db:create RAILS_ENV=production
docker-compose run app rake db:migrate RAILS_ENV=production
docker-compose up
```

## Необходимо задать хранимые процедуры. Из второй консоли:

```
docker exec -it balancesheet_db_1 psql -U postgres

\c balance_sheet

CREATE OR REPLACE FUNCTION create_income (amount decimal, user_id int, transaction_time timestamp) RETURNS void AS $$
  <<fn>>
  DECLARE
      curtime timestamp := now();
  BEGIN
    INSERT INTO "incomes" ("created_at", "amount", "user_id", "transaction_time") VALUES ("curtime", "amount", "user_id", "transaction_time");
  END;
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION create_expense (amount decimal, user_id int, transaction_time timestamp, purpose character varying) RETURNS void AS $$
  <<fn>>
  DECLARE
      curtime timestamp := now();
  BEGIN
    INSERT INTO "expenses" ("created_at", "amount", "user_id", "transaction_time", "purpose") VALUES ("curtime", "amount", "user_id", "transaction_time", "purpose");
  END;
$$
LANGUAGE 'plpgsql';

\q
```

## Также можно сгенерировать фейовые данные в БД

```
docker-compose run app rake gen:users[33]
docker-compose run app rake gen:transactions
```