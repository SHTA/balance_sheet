FactoryBot.define do
  factory :user do
    name {"test_user"}
    address { "СПб, Невский пр., 1" }
    birthday { "1990-05-05" }
  end
end