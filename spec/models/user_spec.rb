require 'rails_helper'

RSpec.describe User, type: :model do
  before { @user = FactoryBot.create(:user) }

  # Присутствие уникального name и birthday
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name).case_insensitive }
  it { should validate_presence_of(:birthday) }

  it 'Проверка корректности метода подсчёта баланса за месяц' do
    2.times { @user.expenses.create({ amount: 2500, transaction_time: Time.now }) }
    @user.get_month_balance.should eq(-5000)
    @user.incomes.create({ amount: 15000, transaction_time: Time.now })
    @user.get_month_balance.should eq(10000)
    # Дальше добавим транзакции вне интересующего нас периода. Сумма баланса за месяц не должна измениться.
    @user.incomes.create({ amount: 15000, transaction_time: (Time.now - 2.month) })
    @user.expenses.create({ amount: 5, transaction_time: (Time.now - 2.month) })
    @user.expenses.create({ amount: 3, transaction_time: (Time.now + 1.month) })
    @user.get_month_balance.should eq(10000)
  end
end
