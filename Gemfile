source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'
gem 'rails', '~> 5.2.0'

# BACK
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'

# FRONT
gem 'bootsnap', '>= 1.1.0', require: false
gem 'bootstrap-sass', '~> 3.4', '>= 3.4.1'
gem 'coffee-rails', '~> 4.2'
gem 'font-awesome-sass', '~> 4.6', '>= 4.6.2'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17', '>= 4.17.37'
gem 'jquery-rails', '~> 4.3', '>= 4.3.3'
gem 'momentjs-rails', '~> 2.20', '>= 2.20.1'
gem 'sass-rails', '~> 5.0', '>= 5.0.7'
gem 'simple_form', '~> 4.1'
gem 'slim-rails', '~> 3.2'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry', '~> 0.10.4'
  gem 'pry-nav', '~> 0.2.4'
  gem 'pry-rails', '~> 0.3.9'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'rubocop', '~> 0.66.0'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_cleaner', '~> 1.7'
  gem 'factory_bot_rails', '~> 5.0', '>= 5.0.2'
  gem 'rspec-rails', '~> 3.8', '>= 3.8.2'
  gem 'shoulda', '~> 3.5'
  gem 'shoulda-matchers', '~> 2.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
