class CreateExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :expenses do |t|
      t.datetime :transaction_time, null: false
      t.decimal :amount, null: false
      t.references :user, foreign_key: true, null: false
      t.string :purpose

      t.datetime :created_at, null: false
    end
  end
end
